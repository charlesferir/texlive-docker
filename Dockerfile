FROM registry.gitlab.com/islandoftex/images/texlive:latest

RUN curl -o /usr/local/texlive/texmf-local/tex/latex/local/datasheet.cls \
    "https://raw.githubusercontent.com/PetteriAimonen/latex-datasheet-template/main/datasheet.cls" && \
    mktexlsr

